FROM python:3
RUN pip install gensim numpy scipy scikit-learn
RUN git clone https://github.com/taku910/crfpp.git
RUN cd crfpp && ./configure && sed -i '/#include "winmain.h"/d' crf_test.cpp && sed -i '/#include "winmain.h"/d' crf_learn.cpp && make install
RUN cd crfpp && cp ./.libs/libcrfpp.so.0.0.0 /usr/lib && cp ./.libs/libcrfpp.so.0 /usr/lib