from nlp import CRFWordSegment
from sklearn.pipeline import Pipeline
from gensim import corpora, models
from utilfile import FileUtil
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer, TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier

x_data = ['ลอยกระทง ทำบุญ ปีนี้ดีใจได้มาที่นี่ซักทีถึงรถจะติดคนจะแน่นไม่หวั่นจ้า',
'ขอโทดนะ',
'สุขสันต์วันลอยกระทงนะแจ๊ะ',
'สุขสันต์วันลอยกระทงนะแจ๊ะ ']

y_train = [1,0,1,1]

x_test = ['ลอยกระทง ทำบุญ ปีนี้ดีใจได้มาที่นี่',
'ขอโทดนะสุขสันต์วันลอยกระทงนะแจ๊ะ',
'สุขสันต์วันลอยกระทงนะแจ๊ะ ขอโทดนะ']

def sep_word(x_data):
    crf = CRFWordSegment()
    x_message_list = []
    for msg in x_data:
        result_word = crf.crfpp(msg)
        result_data = ' '.join(result_word)
        x_message_list.append(result_data) 
    return x_message_list

def text_mining_101(): 
    x_train_msg = sep_word(x_data)
    x_test_msg = sep_word(x_test)

    text_clf = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', RandomForestClassifier())])    
    text_clf = text_clf.fit(x_train_msg, y_train)
    y = text_clf.predict(x_test_msg)
    print(y)

if __name__ == '__main__':
    print('start main')
    text_mining_101()